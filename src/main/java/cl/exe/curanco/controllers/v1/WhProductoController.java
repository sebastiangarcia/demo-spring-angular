package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.WhProductoDTO;
import cl.exe.curanco.api.v1.model.WhProductoListDTO;
import cl.exe.curanco.services.WhProductoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/wh/productos")
public class WhProductoController {

    private final WhProductoService whProductoService;

    public WhProductoController(WhProductoService whProductoService) {
        this.whProductoService = whProductoService;
    }

    @GetMapping
    public ResponseEntity<WhProductoListDTO> getAllWhProductos() {
        return new ResponseEntity<WhProductoListDTO>(
                new WhProductoListDTO(whProductoService.getAllWhProductos()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<WhProductoDTO> getWhProductoById(@PathVariable String id) {
        return new ResponseEntity<WhProductoDTO>(whProductoService.getWhProductoById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<WhProductoDTO> createNewWhProducto(@RequestBody WhProductoDTO whProductoDTO) {
        return new ResponseEntity<WhProductoDTO>(whProductoService.createNewWhProducto(whProductoDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<WhProductoDTO> updateWhProducto(@PathVariable Long id, @RequestBody WhProductoDTO whProductoDTO) {
        return new ResponseEntity<WhProductoDTO>(whProductoService.saveWhProductoByDTO(id, whProductoDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteWhProducto(@PathVariable Long id) {
        whProductoService.deleteWhProductoById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
