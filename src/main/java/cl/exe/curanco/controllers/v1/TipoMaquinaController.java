package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.TipoMaquinaDTO;
import cl.exe.curanco.api.v1.model.TipoMaquinaListDTO;
import cl.exe.curanco.services.TipoMaquinaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/tipoMaquinas")
public class TipoMaquinaController {

    private final TipoMaquinaService tipoMaquinaService;

    public TipoMaquinaController(TipoMaquinaService tipoMaquinaService) {
        this.tipoMaquinaService = tipoMaquinaService;
    }

    @GetMapping
    public ResponseEntity<TipoMaquinaListDTO> getAllTiposMaquina() {
        return new ResponseEntity<TipoMaquinaListDTO>(
                new TipoMaquinaListDTO(tipoMaquinaService.getAllTiposMaquina()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoMaquinaDTO> getTipoMaquinaById(@PathVariable String id) {
        return new ResponseEntity<TipoMaquinaDTO>(tipoMaquinaService.getTipoMaquinaById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TipoMaquinaDTO> createNewTipoMaquina(@RequestBody TipoMaquinaDTO tipoMaquinaDTO) {
        return new ResponseEntity<TipoMaquinaDTO>(tipoMaquinaService.createNewTipoMaquina(tipoMaquinaDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TipoMaquinaDTO> updateTipoMaquina(@PathVariable Long id, @RequestBody TipoMaquinaDTO tipoMaquinaDTO) {
        return new ResponseEntity<TipoMaquinaDTO>(tipoMaquinaService.saveTipoMaquinaByDTO(id, tipoMaquinaDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTipoMaquina(@PathVariable Long id) {
        tipoMaquinaService.deleteTipoMaquinaById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
