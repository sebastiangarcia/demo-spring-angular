package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.MaquinaInsumoDTO;
import cl.exe.curanco.api.v1.model.MaquinaInsumoListDTO;
import cl.exe.curanco.services.MaquinaInsumoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/maquinas/insumos")
public class MaquinaInsumoController {

    private final MaquinaInsumoService maquinaInsumoService;

    public MaquinaInsumoController(MaquinaInsumoService maquinaInsumoService) {
        this.maquinaInsumoService = maquinaInsumoService;
    }

    @GetMapping
    public ResponseEntity<MaquinaInsumoListDTO> getAllMaquinaInsumos() {
        return new ResponseEntity<MaquinaInsumoListDTO>(
                new MaquinaInsumoListDTO(maquinaInsumoService.getAllMaquinaInsumos()), HttpStatus.OK);
    }

    /*@GetMapping("/{id}")
    public ResponseEntity<MaquinaInsumoDTO> getMaquinaInsumoById(@PathVariable String id) {
        return new ResponseEntity<MaquinaInsumoDTO>(maquinaInsumoService.getMaquinaInsumoById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<MaquinaInsumoDTO> createNewMaquinaInsumo(@RequestBody MaquinaInsumoDTO maquinaInsumoDTO) {
        return new ResponseEntity<MaquinaInsumoDTO>(maquinaInsumoService.createNewMaquinaInsumo(maquinaInsumoDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MaquinaInsumoDTO> updateMaquinaInsumo(@PathVariable Long id, @RequestBody MaquinaInsumoDTO maquinaInsumoDTO) {
        return new ResponseEntity<MaquinaInsumoDTO>(maquinaInsumoService.saveMaquinaInsumoByDTO(id, maquinaInsumoDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMaquinaInsumo(@PathVariable Long id) {
        maquinaInsumoService.deleteMaquinaInsumoById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }*/
}
