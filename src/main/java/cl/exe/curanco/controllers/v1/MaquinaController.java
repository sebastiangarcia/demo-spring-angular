package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.*;
import cl.exe.curanco.services.MaquinaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/maquinas")
public class MaquinaController {

    private final MaquinaService maquinaService;

    public MaquinaController(MaquinaService maquinaService) {
        this.maquinaService = maquinaService;
    }

    @GetMapping
    public ResponseEntity<MaquinaListDTO> getAllMaquinas() {
        return new ResponseEntity<MaquinaListDTO>(
                new MaquinaListDTO(maquinaService.getAllMaquinas()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MaquinaDTO> getMaquinaById(@PathVariable String id) {
        return new ResponseEntity<MaquinaDTO>(maquinaService.getMaquinaById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<MaquinaDTO> createNewMaquina(@RequestBody MaquinaDTO maquinaDTO) {
        return new ResponseEntity<MaquinaDTO>(maquinaService.createNewMaquina(maquinaDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MaquinaDTO> updateMaquina(@PathVariable Long id, @RequestBody MaquinaDTO maquinaDTO) {
        return new ResponseEntity<MaquinaDTO>(maquinaService.saveMaquinaByDTO(id, maquinaDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMaquina(@PathVariable Long id) {
        maquinaService.deleteMaquinaById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/{id}/insumos")
    public ResponseEntity<MaquinaInsumoListDTO> getAllInsumos(@PathVariable Long id) {
        return new ResponseEntity<MaquinaInsumoListDTO>(
                new MaquinaInsumoListDTO(maquinaService.getInsumosById(id)), HttpStatus.OK);
    }

    @GetMapping("/{id}/operaciones")
    public ResponseEntity<MaquinaOperacionListDTO> getAllOperaciones(@PathVariable Long id) {
        return new ResponseEntity<MaquinaOperacionListDTO>(
                new MaquinaOperacionListDTO(maquinaService.getOperacionesById(id)), HttpStatus.OK);
    }

    @PostMapping("/insumos")
    public ResponseEntity<MaquinaInsumoDTO> createNewMaquinaInsumo(@RequestBody MaquinaInsumoDTO maquinaInsumoDTO) {
        return new ResponseEntity<MaquinaInsumoDTO>(maquinaService.createNewMaquinaInsumo(maquinaInsumoDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}/insumos/{insumoId}")
    public ResponseEntity<Void> deleteInsumo(@PathVariable Long id, @PathVariable Long insumoId) {
        maquinaService.deleteInsumoById(id, insumoId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
