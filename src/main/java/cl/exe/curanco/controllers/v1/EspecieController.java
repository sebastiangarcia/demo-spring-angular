package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.EspecieDTO;
import cl.exe.curanco.api.v1.model.EspecieListDTO;
import cl.exe.curanco.services.EspecieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/especies")
public class EspecieController {

    private final EspecieService especieService;

    public EspecieController(EspecieService especieService) {
        this.especieService = especieService;
    }

    @GetMapping
    public ResponseEntity<EspecieListDTO> getAllEspecies() {
        return new ResponseEntity<EspecieListDTO>(
                new EspecieListDTO(especieService.getAllEspecies()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EspecieDTO> getEspecieById(@PathVariable String id) {
        return new ResponseEntity<EspecieDTO>(especieService.getEspecieById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EspecieDTO> createNewEspecie(@RequestBody EspecieDTO especieDTO) {
        return new ResponseEntity<EspecieDTO>(especieService.createNewEspecie(especieDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EspecieDTO> updateEspecie(@PathVariable Long id, @RequestBody EspecieDTO especieDTO) {
        return new ResponseEntity<EspecieDTO>(especieService.saveEspecieByDTO(id, especieDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEspecie(@PathVariable Long id) {
        especieService.deleteEspecieById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
