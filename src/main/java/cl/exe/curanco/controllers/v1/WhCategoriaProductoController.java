package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.WhCategoriaProductoDTO;
import cl.exe.curanco.api.v1.model.WhCategoriaProductoListDTO;
import cl.exe.curanco.services.WhCategoriaProductoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/wh/categorias")
public class WhCategoriaProductoController {

    private final WhCategoriaProductoService whCategoriaProductoService;

    public WhCategoriaProductoController(WhCategoriaProductoService whCategoriaProductoService) {
        this.whCategoriaProductoService = whCategoriaProductoService;
    }

    @GetMapping
    public ResponseEntity<WhCategoriaProductoListDTO> getAllWhCategoriaProductos() {
        return new ResponseEntity<WhCategoriaProductoListDTO>(
                new WhCategoriaProductoListDTO(whCategoriaProductoService.getAllWhCategoriaProductos()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<WhCategoriaProductoDTO> getWhCategoriaProductoById(@PathVariable String id) {
        return new ResponseEntity<WhCategoriaProductoDTO>(whCategoriaProductoService.getWhCategoriaProductoById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<WhCategoriaProductoDTO> createNewWhCategoriaProducto(@RequestBody WhCategoriaProductoDTO whCategoriaProductoDTO) {
        return new ResponseEntity<WhCategoriaProductoDTO>(whCategoriaProductoService.createNewWhCategoriaProducto(whCategoriaProductoDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<WhCategoriaProductoDTO> updateWhCategoriaProducto(@PathVariable Long id, @RequestBody WhCategoriaProductoDTO whCategoriaProductoDTO) {
        return new ResponseEntity<WhCategoriaProductoDTO>(whCategoriaProductoService.saveWhCategoriaProductoByDTO(id, whCategoriaProductoDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteWhCategoriaProducto(@PathVariable Long id) {
        whCategoriaProductoService.deleteWhCategoriaProductoById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
