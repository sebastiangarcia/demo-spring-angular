package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.TrabajadorDTO;
import cl.exe.curanco.api.v1.model.TrabajadorListDTO;
import cl.exe.curanco.services.TrabajadorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/trabajadores")
public class TrabajadorController {

    private final TrabajadorService trabajadorService;

    public TrabajadorController(TrabajadorService trabajadorService) {
        this.trabajadorService = trabajadorService;
    }

    @GetMapping
    public ResponseEntity<TrabajadorListDTO> getAllTrabajadores() {
        return new ResponseEntity<TrabajadorListDTO>(
                new TrabajadorListDTO(trabajadorService.getAllTrabajadores()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TrabajadorDTO> getTrabajadorById(@PathVariable String id) {
        return new ResponseEntity<TrabajadorDTO>(trabajadorService.getTrabajadorById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TrabajadorDTO> createNewTrabajador(@RequestBody TrabajadorDTO trabajadorDTO) {
        return new ResponseEntity<TrabajadorDTO>(trabajadorService.createNewTrabajador(trabajadorDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TrabajadorDTO> updateTrabajador(@PathVariable Long id, @RequestBody TrabajadorDTO trabajadorDTO) {
        return new ResponseEntity<TrabajadorDTO>(trabajadorService.saveTrabajadorByDTO(id, trabajadorDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTrabajador(@PathVariable Long id) {
        trabajadorService.deleteTrabajadorById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
