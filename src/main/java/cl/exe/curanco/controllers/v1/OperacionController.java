package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.OperacionDTO;
import cl.exe.curanco.api.v1.model.OperacionListDTO;
import cl.exe.curanco.services.OperacionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/operaciones")
public class OperacionController {

    private final OperacionService operacionService;

    public OperacionController(OperacionService operacionService) {
        this.operacionService = operacionService;
    }

    @GetMapping
    public ResponseEntity<OperacionListDTO> getAllOperacions() {
        return new ResponseEntity<OperacionListDTO>(
                new OperacionListDTO(operacionService.getAllOperacions()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OperacionDTO> getOperacionById(@PathVariable String id) {
        return new ResponseEntity<OperacionDTO>(operacionService.getOperacionById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OperacionDTO> createNewOperacion(@RequestBody OperacionDTO operacionDTO) {
        return new ResponseEntity<OperacionDTO>(operacionService.createNewOperacion(operacionDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<OperacionDTO> updateOperacion(@PathVariable Long id, @RequestBody OperacionDTO operacionDTO) {
        return new ResponseEntity<OperacionDTO>(operacionService.saveOperacionByDTO(id, operacionDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOperacion(@PathVariable Long id) {
        operacionService.deleteOperacionById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
