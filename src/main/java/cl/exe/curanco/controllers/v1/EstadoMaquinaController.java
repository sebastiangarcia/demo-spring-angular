package cl.exe.curanco.controllers.v1;

import cl.exe.curanco.api.v1.model.EstadoMaquinaDTO;
import cl.exe.curanco.api.v1.model.EstadoMaquinaListDTO;
import cl.exe.curanco.services.EstadoMaquinaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/estadoMaquinas")
public class EstadoMaquinaController {

    private final EstadoMaquinaService estadoMaquinaService;

    public EstadoMaquinaController(EstadoMaquinaService estadoMaquinaService) {
        this.estadoMaquinaService = estadoMaquinaService;
    }

    @GetMapping
    public ResponseEntity<EstadoMaquinaListDTO> getAllEstadosMaquina() {
        return new ResponseEntity<EstadoMaquinaListDTO>(
                new EstadoMaquinaListDTO(estadoMaquinaService.getAllEstadosMaquina()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EstadoMaquinaDTO> getEstadoMaquinaById(@PathVariable String id) {
        return new ResponseEntity<EstadoMaquinaDTO>(estadoMaquinaService.getEstadoMaquinaById(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EstadoMaquinaDTO> createNewEstadoMaquina(@RequestBody EstadoMaquinaDTO estadoMaquinaDTO) {
        return new ResponseEntity<EstadoMaquinaDTO>(estadoMaquinaService.createNewEstadoMaquina(estadoMaquinaDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EstadoMaquinaDTO> updateEstadoMaquina(@PathVariable Long id, @RequestBody EstadoMaquinaDTO estadoMaquinaDTO) {
        return new ResponseEntity<EstadoMaquinaDTO>(estadoMaquinaService.saveEstadoMaquinaByDTO(id, estadoMaquinaDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEstadoMaquina(@PathVariable Long id) {
        estadoMaquinaService.deleteEstadoMaquinaById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
