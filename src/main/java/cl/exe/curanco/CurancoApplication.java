package cl.exe.curanco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurancoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurancoApplication.class, args);
	}
}
