package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.OperacionDTO;
import cl.exe.curanco.domain.Operacion;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OperacionMapper {

    OperacionMapper INSTANCE = Mappers.getMapper(OperacionMapper.class);

    OperacionDTO operacionToOperacionDTO(Operacion operacion);
    Operacion operacionDtoToOperacion(OperacionDTO operacionDTO);
}
