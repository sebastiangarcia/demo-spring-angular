package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class MaquinaDTO {

    private Long id;
    private String nombre;
    private Long tipoId;
    private Long estadoId;
    private String serie;
    private String marca;
    private String modelo;
    private Long horometro;
    private Boolean habilitado;
}
