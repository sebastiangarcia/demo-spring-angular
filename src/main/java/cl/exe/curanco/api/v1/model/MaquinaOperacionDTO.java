package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class MaquinaOperacionDTO {
    private Long maquinaId;
    private Long OperacionId;
    private Boolean estado;
}
