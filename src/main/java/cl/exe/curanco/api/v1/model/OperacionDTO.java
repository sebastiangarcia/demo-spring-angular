package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class OperacionDTO {
    private Long id;
    private String nombre;
    private Boolean maquina;
    private Boolean trabajador;
    private Boolean habilitado;
}
