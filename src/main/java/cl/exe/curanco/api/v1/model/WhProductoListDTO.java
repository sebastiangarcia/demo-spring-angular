package cl.exe.curanco.api.v1.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class WhProductoListDTO {

    List<WhProductoDTO> producto;

}
