package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.TrabajadorDTO;
import cl.exe.curanco.domain.Trabajador;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TrabajadorMapper {

    TrabajadorMapper INSTANCE = Mappers.getMapper(TrabajadorMapper.class);

    TrabajadorDTO trabajadorToTrabajadorDTO(Trabajador trabajador);
    Trabajador trabajadorDtoToTrabajador(TrabajadorDTO trabajadorDTO);
}
