package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class WhProductoDTO {
    private Long id;
    private String nombre;
    private Long categoriaId;
    private String sku;
    private String marca;
    private String modelo;
    private Long stock;
    private Long stockMin;
    private Long tarifaCompra;
    private Long tarifaVenta;
    private Boolean habilitado;
}
