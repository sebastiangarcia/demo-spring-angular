package cl.exe.curanco.api.v1.model;

import lombok.Data;

import java.util.List;

@Data
public class EstadoMaquinaDTO {

    private Long id;
    private String nombre;
    private Integer nivel;
    private Boolean habilitado;
}
