package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class WhCategoriaProductoDTO {
    private Long id;
    private String nombre;
    private Boolean servicio;
    private Boolean habilitado;
}
