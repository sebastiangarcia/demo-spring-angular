package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.TipoMaquinaDTO;
import cl.exe.curanco.domain.TipoMaquina;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TipoMaquinaMapper {

    TipoMaquinaMapper INSTANCE = Mappers.getMapper(TipoMaquinaMapper.class);


    TipoMaquinaDTO tipoMaquinaToTipoMaquinaDTO(TipoMaquina tipoMaquina);
    TipoMaquina tipoMaquinaDtoToTipoMaquina(TipoMaquinaDTO tipoMaquinaDTO);
}
