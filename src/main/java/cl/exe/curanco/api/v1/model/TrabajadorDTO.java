package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class TrabajadorDTO {
    private Long id;
    private String rut;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String cargo;
    private String email;
    private Long tarifaSueldo;
    private Long tarifaMeta;
    private Long tarifaHoraExtra;
    private Long tarifaOtros;
    private Boolean habilitado;
}
