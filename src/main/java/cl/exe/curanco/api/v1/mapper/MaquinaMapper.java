package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.MaquinaDTO;
import cl.exe.curanco.domain.Maquina;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MaquinaMapper {

    MaquinaMapper INSTANCE = Mappers.getMapper(MaquinaMapper.class);

    @Mappings({
            @Mapping(source = "maquina.tipo.id", target = "tipoId"),
            @Mapping(source = "maquina.estado.id", target = "estadoId")
    })
    MaquinaDTO maquinaToMaquinaDTO(Maquina maquina);

    @Mappings({
            @Mapping(source = "tipoId", target = "tipo.id"),
            @Mapping(source = "estadoId", target = "estado.id")
    })
    Maquina maquinaDtoToMaquina(MaquinaDTO maquinaDTO);
}
