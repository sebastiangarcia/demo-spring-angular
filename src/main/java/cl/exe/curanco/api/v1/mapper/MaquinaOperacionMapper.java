package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.MaquinaOperacionDTO;
import cl.exe.curanco.domain.MaquinaOperacion;
import cl.exe.curanco.domain.MaquinaOperacion;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MaquinaOperacionMapper {

    MaquinaOperacionMapper INSTANCE = Mappers.getMapper(MaquinaOperacionMapper.class);

    @Mappings({
            @Mapping(source = "maquinaOperacion.maquina.id", target = "maquinaId"),
            @Mapping(source = "maquinaOperacion.operacion.id", target = "operacionId")
    })
    MaquinaOperacionDTO maquinaOperacionToMaquinaOperacionDTO(MaquinaOperacion maquinaOperacion);

    @Mappings({
            @Mapping(source = "maquinaId", target = "maquina.id"),
            @Mapping(source = "operacionId", target = "operacion.id")
    })
    MaquinaOperacion maquinaOperacionDtoToMaquinaOperacion(MaquinaOperacionDTO maquinaOperacionDTO);
}
