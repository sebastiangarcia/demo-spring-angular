package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.WhCategoriaProductoDTO;
import cl.exe.curanco.domain.WhCategoriaProducto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface WhCategoriaProductoMapper {

    WhCategoriaProductoMapper INSTANCE = Mappers.getMapper(WhCategoriaProductoMapper.class);

    WhCategoriaProductoDTO whCategoriaProductoToWhCategoriaProductoDTO(WhCategoriaProducto whCategoriaProducto);
    WhCategoriaProducto whCategoriaProductoDtoToWhCategoriaProducto(WhCategoriaProductoDTO whCategoriaProductoDTO);
}
