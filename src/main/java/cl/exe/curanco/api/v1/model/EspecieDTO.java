package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class EspecieDTO {
    private Long id;
    private String nombre;
    private Boolean habilitado;
}
