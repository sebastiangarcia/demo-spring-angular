package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.WhProductoDTO;
import cl.exe.curanco.domain.WhProducto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface WhProductoMapper {

    WhProductoMapper INSTANCE = Mappers.getMapper(WhProductoMapper.class);

    @Mappings({
            @Mapping(source = "whProducto.categoria.id", target = "categoriaId")
    })
    WhProductoDTO whProductoToWhProductoDTO(WhProducto whProducto);

    @Mappings({
            @Mapping(source = "categoriaId", target = "categoria.id")
    })
    WhProducto whProductoDtoToWhProducto(WhProductoDTO whProductoDTO);
}
