package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.MaquinaInsumoDTO;
import cl.exe.curanco.domain.MaquinaInsumo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MaquinaInsumoMapper {

    MaquinaInsumoMapper INSTANCE = Mappers.getMapper(MaquinaInsumoMapper.class);

    @Mappings({
            @Mapping(source = "maquinaInsumo.maquina.id", target = "maquinaId"),
            @Mapping(source = "maquinaInsumo.producto.id", target = "productoId")
    })
    MaquinaInsumoDTO maquinaInsumoToMaquinaInsumoDTO(MaquinaInsumo maquinaInsumo);

    @Mappings({
            @Mapping(source = "maquinaId", target = "maquina.id"),
            @Mapping(source = "productoId", target = "producto.id")
    })
    MaquinaInsumo maquinaInsumoDtoToMaquinaInsumo(MaquinaInsumoDTO maquinaInsumoDTO);
}
