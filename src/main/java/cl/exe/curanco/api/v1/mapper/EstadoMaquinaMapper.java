package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.EstadoMaquinaDTO;
import cl.exe.curanco.domain.EstadoMaquina;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EstadoMaquinaMapper {

    EstadoMaquinaMapper INSTANCE = Mappers.getMapper(EstadoMaquinaMapper.class);

    EstadoMaquinaDTO estadoMaquinaToEstadoMaquinaDTO(EstadoMaquina estadoMaquina);
    EstadoMaquina estadoMaquinaDtoToEstadoMaquina(EstadoMaquinaDTO estadoMaquinaDTO);
}
