package cl.exe.curanco.api.v1.mapper;

import cl.exe.curanco.api.v1.model.EspecieDTO;
import cl.exe.curanco.domain.Especie;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EspecieMapper {

    EspecieMapper INSTANCE = Mappers.getMapper(EspecieMapper.class);

    EspecieDTO especieToEspecieDTO(Especie especie);
    Especie especieDtoToEspecie(EspecieDTO especieDTO);
}
