package cl.exe.curanco.api.v1.model;

import lombok.Data;

import java.util.List;

@Data
public class TipoMaquinaDTO {

    private Long id;
    private String nombre;
    private Boolean habilitado;
}
