package cl.exe.curanco.api.v1.model;

import lombok.Data;

@Data
public class MaquinaInsumoDTO {
    private Long maquinaId;
    private Long productoId;
    private Boolean estado;
}
