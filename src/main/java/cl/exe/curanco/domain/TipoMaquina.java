package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
public class TipoMaquina {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="tipo_maquina_id_seq")
    @SequenceGenerator(name="tipo_maquina_id_seq", sequenceName = "tipo_maquina_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String nombre;
    private Boolean habilitado;
}
