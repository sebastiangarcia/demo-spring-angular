package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
public class MaquinaOperacionId implements Serializable {

    @Column(name = "maquina_id")
    private Long maquinaId;

    @Column(name = "operacion_id")
    private Long operacionId;

    private MaquinaOperacionId() {}

    public MaquinaOperacionId(Long maquinaId, Long operacionId) {
        this.maquinaId = maquinaId;
        this.operacionId = operacionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        MaquinaOperacionId that = (MaquinaOperacionId) o;
        return Objects.equals(maquinaId, that.maquinaId) &&
                Objects.equals(operacionId, that.operacionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maquinaId, operacionId);
    }
}
