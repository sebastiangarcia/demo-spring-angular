package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
public class EstadoMaquina {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="estado_maquina_id_seq")
    @SequenceGenerator(name="estado_maquina_id_seq", sequenceName = "estado_maquina_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String nombre;
    private Integer nivel;
    private Boolean habilitado;

    @OneToMany(mappedBy = "estado")
    private List<Maquina> maquinas;
}
