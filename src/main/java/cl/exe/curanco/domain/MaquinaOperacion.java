package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "maquina_operacion")
public class MaquinaOperacion {

    @EmbeddedId
    private MaquinaOperacionId id;

    @ManyToOne(fetch=FetchType.LAZY)
    @MapsId("maquinaId")
    private Maquina maquina;


    @ManyToOne(fetch=FetchType.LAZY)
    @MapsId("operacionId")
    private Operacion operacion;

    private Boolean estado;

    public MaquinaOperacion(){}

    public MaquinaOperacion(Maquina maquina, Operacion operacion) {
        this.maquina = maquina;
        this.operacion = operacion;
        this.id = new MaquinaOperacionId(maquina.getId(), operacion.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        MaquinaOperacion that = (MaquinaOperacion) o;
        return Objects.equals(maquina, that.maquina) &&
                Objects.equals(operacion, that.operacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maquina, operacion);
    }
}
