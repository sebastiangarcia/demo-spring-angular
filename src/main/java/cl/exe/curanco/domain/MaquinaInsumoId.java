package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
public class MaquinaInsumoId implements Serializable {

    @Column(name = "maquina_id")
    private Long maquinaId;

    @Column(name = "producto_id")
    private Long productoId;

    private MaquinaInsumoId() {}

    public MaquinaInsumoId(Long maquinaId, Long productoId) {
        this.maquinaId = maquinaId;
        this.productoId = productoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        MaquinaInsumoId that = (MaquinaInsumoId) o;
        return Objects.equals(maquinaId, that.maquinaId) &&
                Objects.equals(productoId, that.productoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maquinaId, productoId);
    }
}
