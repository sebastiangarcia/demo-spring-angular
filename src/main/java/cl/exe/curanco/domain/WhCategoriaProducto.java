package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class WhCategoriaProducto {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="wh_categoria_producto_id_seq")
    @SequenceGenerator(name="wh_categoria_producto_id_seq", sequenceName = "wh_categoria_producto_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String nombre;
    private Boolean servicio;
    private Boolean habilitado;
}
