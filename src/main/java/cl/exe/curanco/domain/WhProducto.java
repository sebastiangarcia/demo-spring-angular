package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class WhProducto {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="wh_producto_id_seq")
    @SequenceGenerator(name="wh_producto_id_seq", sequenceName = "wh_producto_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String nombre;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "categoria_id")
    private WhCategoriaProducto categoria;

    private String sku;
    private String marca;
    private String modelo;
    private Long stock;

    @Column(name = "stock_min")
    private Long stockMin;

    @Column(name = "tarifa_compra")
    private Long tarifaCompra;

    @Column(name = "tarifa_venta")
    private Long tarifaVenta;

    private Boolean habilitado;

    @OneToMany(mappedBy = "producto", cascade = CascadeType.ALL)
    private List<MaquinaInsumo> maquinas = new ArrayList<>();
    //@JoinTable(name = "maquina_insumo", joinColumns = @JoinColumn(name = "producto_id"), inverseJoinColumns = @JoinColumn(name = "maquina_id"))

}
