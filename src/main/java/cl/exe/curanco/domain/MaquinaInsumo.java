package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "maquina_insumo")
public class MaquinaInsumo {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "maquinaId", column = @Column(name = "maquina_id", nullable = false)),
            @AttributeOverride(name = "productoId", column = @Column(name = "producto_id", nullable = false))
    })
    private MaquinaInsumoId id;

    @ManyToOne(fetch=FetchType.LAZY)
    @MapsId("maquinaId")
    private Maquina maquina;


    @ManyToOne(fetch=FetchType.LAZY)
    @MapsId("productoId")
    private WhProducto producto;

    private Boolean estado;

    public MaquinaInsumo(){}

    public MaquinaInsumo(Maquina maquina, WhProducto producto) {
        this.maquina = maquina;
        this.producto = producto;
        this.id = new MaquinaInsumoId(maquina.getId(), producto.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        MaquinaInsumo that = (MaquinaInsumo) o;
        return Objects.equals(maquina, that.maquina) &&
                Objects.equals(producto, that.producto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maquina, producto);
    }
}
