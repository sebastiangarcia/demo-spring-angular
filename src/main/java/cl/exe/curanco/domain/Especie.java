package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Especie {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="especie_id_seq")
    @SequenceGenerator(name="especie_id_seq", sequenceName = "especie_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String nombre;
    private Boolean habilitado;
}
