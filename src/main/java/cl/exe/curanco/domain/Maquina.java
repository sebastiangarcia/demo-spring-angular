package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
public class Maquina {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="maquina_id_seq")
    @SequenceGenerator(name="maquina_id_seq", sequenceName = "maquina_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String nombre;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "tipo_id")
    private TipoMaquina tipo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "estado_id")
    private EstadoMaquina estado;

    private String serie;
    private String marca;
    private String modelo;
    private Long horometro;
    private Boolean habilitado;

    @OneToMany(mappedBy = "maquina", cascade = CascadeType.ALL)
    private List<MaquinaInsumo> insumos = new ArrayList<>();

    @OneToMany(mappedBy = "maquina", cascade = CascadeType.ALL)
    private List<MaquinaOperacion> operaciones = new ArrayList<>();

    public void addInsumo(WhProducto producto) {
        MaquinaInsumo maquinaInsumo = new MaquinaInsumo(this, producto);
        insumos.add(maquinaInsumo);
        producto.getMaquinas().add(maquinaInsumo);
    }

    public void removeInsumo(WhProducto producto) {
        for (Iterator<MaquinaInsumo> iterator = insumos.iterator();
             iterator.hasNext(); ) {
            MaquinaInsumo maquinaInsumo = iterator.next();

            if (maquinaInsumo.getMaquina().equals(this) &&
                    maquinaInsumo.getProducto().equals(producto)) {
                iterator.remove();
                maquinaInsumo.getProducto().getMaquinas().remove(maquinaInsumo);
                maquinaInsumo.setMaquina(null);
                maquinaInsumo.setProducto(null);
            }
        }
    }
}
