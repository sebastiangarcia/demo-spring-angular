package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Trabajador {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="trabajador_id_seq")
    @SequenceGenerator(name="trabajador_id_seq", sequenceName = "trabajador_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String rut;
    @Column(name = "apellido_paterno")
    private String apellidoPaterno;
    @Column(name = "apellido_materno")
    private String apellidoMaterno;
    private String nombres;
    private String cargo;
    private String email;
    @Column(name = "tarifa_sueldo")
    private Long tarifaSueldo;
    @Column(name = "tarifa_meta")
    private Long tarifaMeta;
    @Column(name = "tarifa_hora_extra")
    private Long tarifaHoraExtra;
    @Column(name = "tarifa_otros")
    private Long tarifaOtros;
    private Boolean habilitado;
}
