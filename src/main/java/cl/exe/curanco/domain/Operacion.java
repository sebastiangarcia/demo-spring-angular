package cl.exe.curanco.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Operacion {

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="operacion_id_seq")
    @SequenceGenerator(name="operacion_id_seq", sequenceName = "operacion_id_seq", allocationSize=1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    private String nombre;
    private Boolean maquina;
    private Boolean trabajador;
    private Boolean habilitado;

    @OneToMany(mappedBy = "operacion", cascade = CascadeType.ALL)
    private List<MaquinaOperacion> maquinas = new ArrayList<>();
}
