package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.OperacionDTO;

import java.util.List;

public interface OperacionService {

    List<OperacionDTO> getAllOperacions();

    OperacionDTO getOperacionById(Long id);
    OperacionDTO getOperacionByNombre(String nombre);
    OperacionDTO createNewOperacion(OperacionDTO operacionDTO);
    OperacionDTO saveOperacionByDTO(Long id, OperacionDTO operacionDTO);
    void deleteOperacionById(Long id);
}
