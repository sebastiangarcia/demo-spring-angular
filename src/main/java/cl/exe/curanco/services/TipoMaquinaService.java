package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.TipoMaquinaDTO;

import java.util.List;

public interface TipoMaquinaService {

    List<TipoMaquinaDTO> getAllTiposMaquina();

    TipoMaquinaDTO getTipoMaquinaById(Long id);
    TipoMaquinaDTO getTipoMaquinaByNombre(String nombre);
    TipoMaquinaDTO createNewTipoMaquina(TipoMaquinaDTO tipoMaquinaDTO);
    TipoMaquinaDTO saveTipoMaquinaByDTO(Long id, TipoMaquinaDTO tipoMaquinaDTO);
    void deleteTipoMaquinaById(Long id);
}
