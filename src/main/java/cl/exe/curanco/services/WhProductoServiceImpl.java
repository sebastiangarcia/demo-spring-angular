package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.WhProductoMapper;
import cl.exe.curanco.api.v1.model.WhProductoDTO;
import cl.exe.curanco.domain.WhProducto;
import cl.exe.curanco.repositories.WhProductoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WhProductoServiceImpl implements WhProductoService {

    private final WhProductoMapper whProductoMapper;
    private final WhProductoRepository whProductoRepository;

    public WhProductoServiceImpl(WhProductoMapper whProductoMapper, WhProductoRepository whProductoRepository) {
        this.whProductoMapper = whProductoMapper;
        this.whProductoRepository = whProductoRepository;
    }

    @Override
    public List<WhProductoDTO> getAllWhProductos() {

        return whProductoRepository.findAll()
                .stream()
                .map(whProductoMapper::whProductoToWhProductoDTO)
                .collect(Collectors.toList());
    }

    @Override
    public WhProductoDTO getWhProductoById(Long id) {
        //return whProductoMapper.whProductoToWhProductoDTO(whProductoRepository.findById(id));
        return whProductoRepository.findById(id)
                .map(whProductoMapper::whProductoToWhProductoDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public WhProductoDTO getWhProductoByNombre(String nombre) {
        return whProductoMapper.whProductoToWhProductoDTO(whProductoRepository.findByNombre(nombre));
    }

    @Override
    public WhProductoDTO createNewWhProducto(WhProductoDTO whProductoDTO) {
        WhProducto whProducto = whProductoMapper.whProductoDtoToWhProducto(whProductoDTO);
        return saveAndReturnDTO(whProducto);
    }

    private WhProductoDTO saveAndReturnDTO(WhProducto whProducto) {
        WhProducto savedWhProducto = whProductoRepository.save(whProducto);
        return whProductoMapper.whProductoToWhProductoDTO(savedWhProducto);
    }

    @Override
    public WhProductoDTO saveWhProductoByDTO(Long id, WhProductoDTO whProductoDTO) {
        WhProducto whProducto = whProductoMapper.whProductoDtoToWhProducto(whProductoDTO);
        whProducto.setId(id);
        return saveAndReturnDTO(whProducto);
    }

    @Override
    public void deleteWhProductoById(Long id) {
        whProductoRepository.deleteById(id);
    }

}
