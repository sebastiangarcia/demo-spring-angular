package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.MaquinaInsumoDTO;

import java.util.List;

public interface MaquinaInsumoService {

    List<MaquinaInsumoDTO> getAllMaquinaInsumos();

    MaquinaInsumoDTO getMaquinaInsumoById(Long id);
    MaquinaInsumoDTO createNewMaquinaInsumo(MaquinaInsumoDTO maquinaInsumoDTO);
    MaquinaInsumoDTO saveMaquinaInsumoByDTO(Long id, MaquinaInsumoDTO maquinaInsumoDTO);
    void deleteMaquinaInsumoById(Long id);
}
