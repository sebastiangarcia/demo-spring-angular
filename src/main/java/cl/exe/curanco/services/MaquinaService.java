package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.MaquinaDTO;
import cl.exe.curanco.api.v1.model.MaquinaInsumoDTO;
import cl.exe.curanco.api.v1.model.MaquinaOperacionDTO;

import java.util.List;

public interface MaquinaService {

    List<MaquinaDTO> getAllMaquinas();

    MaquinaDTO getMaquinaById(Long id);
    MaquinaDTO getMaquinaByNombre(String nombre);
    MaquinaDTO createNewMaquina(MaquinaDTO maquinaDTO);
    MaquinaDTO saveMaquinaByDTO(Long id, MaquinaDTO maquinaDTO);
    void deleteMaquinaById(Long id);

    List<MaquinaInsumoDTO> getInsumosById(Long id);
    List<MaquinaOperacionDTO> getOperacionesById(Long id);
    MaquinaInsumoDTO createNewMaquinaInsumo(MaquinaInsumoDTO maquinaInsumoDTO);
    void deleteInsumoById(Long id, Long insumoId);
}
