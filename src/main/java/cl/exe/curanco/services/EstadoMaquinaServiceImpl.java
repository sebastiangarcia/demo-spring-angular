package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.EstadoMaquinaMapper;
import cl.exe.curanco.api.v1.model.EstadoMaquinaDTO;
import cl.exe.curanco.domain.EstadoMaquina;
import cl.exe.curanco.repositories.EstadoMaquinaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstadoMaquinaServiceImpl implements EstadoMaquinaService {

    private final EstadoMaquinaMapper estadoMaquinaMapper;
    private final EstadoMaquinaRepository estadoMaquinaRepository;

    public EstadoMaquinaServiceImpl(EstadoMaquinaMapper estadoMaquinaMapper, EstadoMaquinaRepository estadoMaquinaRepository) {
        this.estadoMaquinaMapper = estadoMaquinaMapper;
        this.estadoMaquinaRepository = estadoMaquinaRepository;
    }

    @Override
    public List<EstadoMaquinaDTO> getAllEstadosMaquina() {

        return estadoMaquinaRepository.findAll()
                .stream()
                .map(estadoMaquinaMapper::estadoMaquinaToEstadoMaquinaDTO)
                .collect(Collectors.toList());
    }

    @Override
    public EstadoMaquinaDTO getEstadoMaquinaById(Long id) {
        return estadoMaquinaRepository.findById(id)
                .map(estadoMaquinaMapper::estadoMaquinaToEstadoMaquinaDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public EstadoMaquinaDTO getEstadoMaquinaByNombre(String nombre) {
        return estadoMaquinaMapper.estadoMaquinaToEstadoMaquinaDTO(estadoMaquinaRepository.findByNombre(nombre));
    }

    @Override
    public EstadoMaquinaDTO createNewEstadoMaquina(EstadoMaquinaDTO estadoMaquinaDTO) {
        EstadoMaquina estadoMaquina = estadoMaquinaMapper.estadoMaquinaDtoToEstadoMaquina(estadoMaquinaDTO);
        return saveAndReturnDTO(estadoMaquina);
    }

    private EstadoMaquinaDTO saveAndReturnDTO(EstadoMaquina estadoMaquina) {
        EstadoMaquina savedEstadoMaquina = estadoMaquinaRepository.save(estadoMaquina);
        return estadoMaquinaMapper.estadoMaquinaToEstadoMaquinaDTO(savedEstadoMaquina);
    }

    @Override
    public EstadoMaquinaDTO saveEstadoMaquinaByDTO(Long id, EstadoMaquinaDTO estadoMaquinaDTO) {
        EstadoMaquina estadoMaquina = estadoMaquinaMapper.estadoMaquinaDtoToEstadoMaquina(estadoMaquinaDTO);
        estadoMaquina.setId(id);
        return saveAndReturnDTO(estadoMaquina);
    }

    @Override
    public void deleteEstadoMaquinaById(Long id) {
        estadoMaquinaRepository.deleteById(id);
    }

}
