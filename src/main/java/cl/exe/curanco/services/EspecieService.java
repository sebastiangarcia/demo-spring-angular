package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.EspecieDTO;

import java.util.List;

public interface EspecieService {

    List<EspecieDTO> getAllEspecies();

    EspecieDTO getEspecieById(Long id);
    EspecieDTO getEspecieByNombre(String nombre);
    EspecieDTO createNewEspecie(EspecieDTO especieDTO);
    EspecieDTO saveEspecieByDTO(Long id, EspecieDTO especieDTO);
    void deleteEspecieById(Long id);
}
