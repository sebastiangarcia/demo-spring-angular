package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.MaquinaInsumoMapper;
import cl.exe.curanco.api.v1.mapper.MaquinaMapper;
import cl.exe.curanco.api.v1.mapper.MaquinaOperacionMapper;
import cl.exe.curanco.api.v1.model.MaquinaDTO;
import cl.exe.curanco.api.v1.model.MaquinaInsumoDTO;
import cl.exe.curanco.api.v1.model.MaquinaOperacionDTO;
import cl.exe.curanco.domain.Maquina;
import cl.exe.curanco.domain.MaquinaInsumo;
import cl.exe.curanco.domain.MaquinaInsumoId;
import cl.exe.curanco.repositories.MaquinaInsumoRepository;
import cl.exe.curanco.repositories.MaquinaOperacionRepository;
import cl.exe.curanco.repositories.MaquinaRepository;
import cl.exe.curanco.repositories.WhProductoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MaquinaServiceImpl implements MaquinaService {

    private final MaquinaMapper maquinaMapper;
    private final MaquinaInsumoMapper maquinaInsumoMapper;
    private final MaquinaOperacionMapper maquinaOperacionMapper;
    private final MaquinaRepository maquinaRepository;
    private final WhProductoRepository whProductoRepository;
    private final MaquinaInsumoRepository maquinaInsumoRepository;
    private final MaquinaOperacionRepository maquinaOperacionRepository;

    public MaquinaServiceImpl(MaquinaMapper maquinaMapper, MaquinaInsumoMapper maquinaInsumoMapper, MaquinaOperacionMapper maquinaOperacionMapper,
                              MaquinaRepository maquinaRepository, WhProductoRepository whProductoRepository, MaquinaInsumoRepository maquinaInsumoRepository, MaquinaOperacionRepository maquinaOperacionRepository) {
        this.maquinaMapper = maquinaMapper;
        this.maquinaInsumoMapper = maquinaInsumoMapper;
        this.maquinaOperacionMapper = maquinaOperacionMapper;
        this.maquinaRepository = maquinaRepository;
        this.whProductoRepository = whProductoRepository;
        this.maquinaInsumoRepository = maquinaInsumoRepository;
        this.maquinaOperacionRepository = maquinaOperacionRepository;
    }

    @Override
    public List<MaquinaDTO> getAllMaquinas() {

        return maquinaRepository.findAll()
                .stream()
                .map(maquinaMapper::maquinaToMaquinaDTO)
                .collect(Collectors.toList());
    }

    @Override
    public MaquinaDTO getMaquinaById(Long id) {
        //return maquinaMapper.maquinaToMaquinaDTO(maquinaRepository.findById(id));
        return maquinaRepository.findById(id)
                .map(maquinaMapper::maquinaToMaquinaDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public MaquinaDTO getMaquinaByNombre(String nombre) {
        return maquinaMapper.maquinaToMaquinaDTO(maquinaRepository.findByNombre(nombre));
    }

    @Override
    public MaquinaDTO createNewMaquina(MaquinaDTO maquinaDTO) {
        Maquina maquina = maquinaMapper.maquinaDtoToMaquina(maquinaDTO);
        return saveAndReturnDTO(maquina);
    }

    private MaquinaDTO saveAndReturnDTO(Maquina maquina) {
        Maquina savedMaquina = maquinaRepository.save(maquina);
        return maquinaMapper.maquinaToMaquinaDTO(savedMaquina);
    }

    @Override
    public MaquinaDTO saveMaquinaByDTO(Long id, MaquinaDTO maquinaDTO) {
        Maquina maquina = maquinaMapper.maquinaDtoToMaquina(maquinaDTO);
        maquina.setId(id);
        return saveAndReturnDTO(maquina);
    }

    @Override
    public void deleteMaquinaById(Long id) {
        maquinaRepository.deleteById(id);
    }

    @Override
    public List<MaquinaInsumoDTO> getInsumosById(Long id){

        return maquinaRepository.getOne(id).getInsumos()
                .stream()
                .map(maquinaInsumoMapper::maquinaInsumoToMaquinaInsumoDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<MaquinaOperacionDTO> getOperacionesById(Long id){

        return maquinaRepository.getOne(id).getOperaciones()
                .stream()
                .map(maquinaOperacionMapper::maquinaOperacionToMaquinaOperacionDTO)
                .collect(Collectors.toList());
    }

    @Override
    public MaquinaInsumoDTO createNewMaquinaInsumo(MaquinaInsumoDTO maquinaInsumoDTO) {
        //MaquinaInsumo maquinaInsumo = maquinaInsumoMapper.maquinaInsumoDtoToMaquinaInsumo(maquinaInsumoDTO);
        //MaquinaInsumo maquinaInsumo = maquinaInsumoRepository.ge
        MaquinaInsumo maquinaInsumo = new MaquinaInsumo(maquinaRepository.getOne(maquinaInsumoDTO.getMaquinaId()), whProductoRepository.getOne(maquinaInsumoDTO.getProductoId()));
        return saveInsumoAndReturnDTO(maquinaInsumo);
        //return saveInsumoAndReturnDTO(new MaquinaInsumo(maquinaRepository.getOne(maquinaInsumoDTO.getMaquinaId()), whProductoRepository.getOne(maquinaInsumoDTO.getProductoId())));
    }

    private MaquinaInsumoDTO saveInsumoAndReturnDTO(MaquinaInsumo maquinaInsumo) {
        MaquinaInsumo savedMaquinaInsumo = maquinaInsumoRepository.save(maquinaInsumo);
        return maquinaInsumoMapper.maquinaInsumoToMaquinaInsumoDTO(savedMaquinaInsumo);
    }

    @Override
    public void deleteInsumoById(Long id, Long insumoId) {
        maquinaInsumoRepository.delete(new MaquinaInsumo(maquinaRepository.getOne(id), whProductoRepository.getOne(insumoId)));
    }

}
