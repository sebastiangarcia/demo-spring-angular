package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.TipoMaquinaMapper;
import cl.exe.curanco.api.v1.model.TipoMaquinaDTO;
import cl.exe.curanco.domain.TipoMaquina;
import cl.exe.curanco.repositories.TipoMaquinaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TipoMaquinaServiceImpl implements TipoMaquinaService {

    private final TipoMaquinaMapper tipoMaquinaMapper;
    private final TipoMaquinaRepository tipoMaquinaRepository;

    public TipoMaquinaServiceImpl(TipoMaquinaMapper tipoMaquinaMapper, TipoMaquinaRepository tipoMaquinaRepository) {
        this.tipoMaquinaMapper = tipoMaquinaMapper;
        this.tipoMaquinaRepository = tipoMaquinaRepository;
    }

    @Override
    public List<TipoMaquinaDTO> getAllTiposMaquina() {

        return tipoMaquinaRepository.findAll()
                .stream()
                .map(tipoMaquinaMapper::tipoMaquinaToTipoMaquinaDTO)
                .collect(Collectors.toList());
    }

    @Override
    public TipoMaquinaDTO getTipoMaquinaById(Long id) {
        //return tipoMaquinaMapper.tipoMaquinaToTipoMaquinaDTO(tipoMaquinaRepository.findById(id));
        return tipoMaquinaRepository.findById(id)
                .map(tipoMaquinaMapper::tipoMaquinaToTipoMaquinaDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public TipoMaquinaDTO getTipoMaquinaByNombre(String nombre) {
        return tipoMaquinaMapper.tipoMaquinaToTipoMaquinaDTO(tipoMaquinaRepository.findByNombre(nombre));
    }

    @Override
    public TipoMaquinaDTO createNewTipoMaquina(TipoMaquinaDTO tipoMaquinaDTO) {
        TipoMaquina tipoMaquina = tipoMaquinaMapper.tipoMaquinaDtoToTipoMaquina(tipoMaquinaDTO);
        return saveAndReturnDTO(tipoMaquina);
    }

    private TipoMaquinaDTO saveAndReturnDTO(TipoMaquina tipoMaquina) {
        TipoMaquina savedTipoMaquina = tipoMaquinaRepository.save(tipoMaquina);
        return tipoMaquinaMapper.tipoMaquinaToTipoMaquinaDTO(savedTipoMaquina);
    }

    @Override
    public TipoMaquinaDTO saveTipoMaquinaByDTO(Long id, TipoMaquinaDTO tipoMaquinaDTO) {
        TipoMaquina tipoMaquina = tipoMaquinaMapper.tipoMaquinaDtoToTipoMaquina(tipoMaquinaDTO);
        tipoMaquina.setId(id);
        return saveAndReturnDTO(tipoMaquina);
    }

    @Override
    public void deleteTipoMaquinaById(Long id) {
        tipoMaquinaRepository.deleteById(id);
    }

}
