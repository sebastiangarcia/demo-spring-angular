package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.WhCategoriaProductoDTO;

import java.util.List;

public interface WhCategoriaProductoService {

    List<WhCategoriaProductoDTO> getAllWhCategoriaProductos();

    WhCategoriaProductoDTO getWhCategoriaProductoById(Long id);
    WhCategoriaProductoDTO getWhCategoriaProductoByNombre(String nombre);
    WhCategoriaProductoDTO createNewWhCategoriaProducto(WhCategoriaProductoDTO whCategoriaProductoDTO);
    WhCategoriaProductoDTO saveWhCategoriaProductoByDTO(Long id, WhCategoriaProductoDTO whCategoriaProductoDTO);
    void deleteWhCategoriaProductoById(Long id);
}
