package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.TrabajadorDTO;

import java.util.List;

public interface TrabajadorService {

    List<TrabajadorDTO> getAllTrabajadores();

    TrabajadorDTO getTrabajadorById(Long id);
    TrabajadorDTO getTrabajadorByRut(String rut);
    TrabajadorDTO createNewTrabajador(TrabajadorDTO trabajadorDTO);
    TrabajadorDTO saveTrabajadorByDTO(Long id, TrabajadorDTO trabajadorDTO);
    void deleteTrabajadorById(Long id);
}
