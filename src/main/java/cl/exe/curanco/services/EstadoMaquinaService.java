package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.EstadoMaquinaDTO;

import java.util.List;

public interface EstadoMaquinaService {

    List<EstadoMaquinaDTO> getAllEstadosMaquina();

    EstadoMaquinaDTO getEstadoMaquinaById(Long id);
    EstadoMaquinaDTO getEstadoMaquinaByNombre(String nombre);
    EstadoMaquinaDTO createNewEstadoMaquina(EstadoMaquinaDTO estadoMaquinaDTO);
    EstadoMaquinaDTO saveEstadoMaquinaByDTO(Long id, EstadoMaquinaDTO estadoMaquinaDTO);
    void deleteEstadoMaquinaById(Long id);
}
