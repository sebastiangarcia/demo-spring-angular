package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.MaquinaInsumoMapper;
import cl.exe.curanco.api.v1.model.MaquinaInsumoDTO;
import cl.exe.curanco.domain.MaquinaInsumo;
import cl.exe.curanco.repositories.MaquinaInsumoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MaquinaInsumoServiceImpl implements MaquinaInsumoService {

    private final MaquinaInsumoMapper maquinaInsumoMapper;
    private final MaquinaInsumoRepository maquinaInsumoRepository;

    public MaquinaInsumoServiceImpl(MaquinaInsumoMapper maquinaInsumoMapper, MaquinaInsumoRepository maquinaInsumoRepository) {
        this.maquinaInsumoMapper = maquinaInsumoMapper;
        this.maquinaInsumoRepository = maquinaInsumoRepository;
    }

    @Override
    public List<MaquinaInsumoDTO> getAllMaquinaInsumos() {

        return maquinaInsumoRepository.findAll()
                .stream()
                .map(maquinaInsumoMapper::maquinaInsumoToMaquinaInsumoDTO)
                .collect(Collectors.toList());
    }

    @Override
    public MaquinaInsumoDTO getMaquinaInsumoById(Long id) {
        //return maquinaInsumoMapper.maquinaInsumoToMaquinaInsumoDTO(maquinaInsumoRepository.findById(id));
        return maquinaInsumoRepository.findById(id)
                .map(maquinaInsumoMapper::maquinaInsumoToMaquinaInsumoDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public MaquinaInsumoDTO createNewMaquinaInsumo(MaquinaInsumoDTO maquinaInsumoDTO) {
        MaquinaInsumo maquinaInsumo = maquinaInsumoMapper.maquinaInsumoDtoToMaquinaInsumo(maquinaInsumoDTO);
        return saveAndReturnDTO(maquinaInsumo);
    }

    private MaquinaInsumoDTO saveAndReturnDTO(MaquinaInsumo maquinaInsumo) {
        MaquinaInsumo savedMaquinaInsumo = maquinaInsumoRepository.save(maquinaInsumo);
        return maquinaInsumoMapper.maquinaInsumoToMaquinaInsumoDTO(savedMaquinaInsumo);
    }

    @Override
    public MaquinaInsumoDTO saveMaquinaInsumoByDTO(Long id, MaquinaInsumoDTO maquinaInsumoDTO) {
        MaquinaInsumo maquinaInsumo = maquinaInsumoMapper.maquinaInsumoDtoToMaquinaInsumo(maquinaInsumoDTO);
        //maquinaInsumo.setId(id);
        return saveAndReturnDTO(maquinaInsumo);
    }

    @Override
    public void deleteMaquinaInsumoById(Long id) {
        maquinaInsumoRepository.deleteById(id);
    }

}
