package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.model.WhProductoDTO;

import java.util.List;

public interface WhProductoService {

    List<WhProductoDTO> getAllWhProductos();

    WhProductoDTO getWhProductoById(Long id);
    WhProductoDTO getWhProductoByNombre(String nombre);
    WhProductoDTO createNewWhProducto(WhProductoDTO whProductoDTO);
    WhProductoDTO saveWhProductoByDTO(Long id, WhProductoDTO whProductoDTO);
    void deleteWhProductoById(Long id);
}
