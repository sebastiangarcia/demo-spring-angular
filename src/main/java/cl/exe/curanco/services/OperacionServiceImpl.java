package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.OperacionMapper;
import cl.exe.curanco.api.v1.model.OperacionDTO;
import cl.exe.curanco.domain.Operacion;
import cl.exe.curanco.repositories.OperacionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OperacionServiceImpl implements OperacionService {

    private final OperacionMapper operacionMapper;
    private final OperacionRepository operacionRepository;

    public OperacionServiceImpl(OperacionMapper operacionMapper, OperacionRepository operacionRepository) {
        this.operacionMapper = operacionMapper;
        this.operacionRepository = operacionRepository;
    }

    @Override
    public List<OperacionDTO> getAllOperacions() {

        return operacionRepository.findAll()
                .stream()
                .map(operacionMapper::operacionToOperacionDTO)
                .collect(Collectors.toList());
    }

    @Override
    public OperacionDTO getOperacionById(Long id) {
        //return operacionMapper.operacionToOperacionDTO(operacionRepository.findById(id));
        return operacionRepository.findById(id)
                .map(operacionMapper::operacionToOperacionDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public OperacionDTO getOperacionByNombre(String nombre) {
        return operacionMapper.operacionToOperacionDTO(operacionRepository.findByNombre(nombre));
    }

    @Override
    public OperacionDTO createNewOperacion(OperacionDTO operacionDTO) {
        Operacion operacion = operacionMapper.operacionDtoToOperacion(operacionDTO);
        return saveAndReturnDTO(operacion);
    }

    private OperacionDTO saveAndReturnDTO(Operacion operacion) {
        Operacion savedOperacion = operacionRepository.save(operacion);
        return operacionMapper.operacionToOperacionDTO(savedOperacion);
    }

    @Override
    public OperacionDTO saveOperacionByDTO(Long id, OperacionDTO operacionDTO) {
        Operacion operacion = operacionMapper.operacionDtoToOperacion(operacionDTO);
        operacion.setId(id);
        return saveAndReturnDTO(operacion);
    }

    @Override
    public void deleteOperacionById(Long id) {
        operacionRepository.deleteById(id);
    }

}
