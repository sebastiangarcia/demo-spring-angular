package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.TrabajadorMapper;
import cl.exe.curanco.api.v1.model.TrabajadorDTO;
import cl.exe.curanco.domain.Trabajador;
import cl.exe.curanco.repositories.TrabajadorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrabajadorServiceImpl implements TrabajadorService {

    private final TrabajadorMapper trabajadorMapper;
    private final TrabajadorRepository trabajadorRepository;

    public TrabajadorServiceImpl(TrabajadorMapper trabajadorMapper, TrabajadorRepository trabajadorRepository) {
        this.trabajadorMapper = trabajadorMapper;
        this.trabajadorRepository = trabajadorRepository;
    }

    @Override
    public List<TrabajadorDTO> getAllTrabajadores() {

        return trabajadorRepository.findAll()
                .stream()
                .map(trabajadorMapper::trabajadorToTrabajadorDTO)
                .collect(Collectors.toList());
    }

    @Override
    public TrabajadorDTO getTrabajadorById(Long id) {
        //return trabajadorMapper.trabajadorToTrabajadorDTO(trabajadorRepository.findById(id));
        return trabajadorRepository.findById(id)
                .map(trabajadorMapper::trabajadorToTrabajadorDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public TrabajadorDTO getTrabajadorByRut(String rut) {
        return trabajadorMapper.trabajadorToTrabajadorDTO(trabajadorRepository.findByRut(rut));
    }

    @Override
    public TrabajadorDTO createNewTrabajador(TrabajadorDTO trabajadorDTO) {
        Trabajador trabajador = trabajadorMapper.trabajadorDtoToTrabajador(trabajadorDTO);
        return saveAndReturnDTO(trabajador);
    }

    private TrabajadorDTO saveAndReturnDTO(Trabajador trabajador) {
        Trabajador savedTrabajador = trabajadorRepository.save(trabajador);
        return trabajadorMapper.trabajadorToTrabajadorDTO(savedTrabajador);
    }

    @Override
    public TrabajadorDTO saveTrabajadorByDTO(Long id, TrabajadorDTO trabajadorDTO) {
        Trabajador trabajador = trabajadorMapper.trabajadorDtoToTrabajador(trabajadorDTO);
        trabajador.setId(id);
        return saveAndReturnDTO(trabajador);
    }

    @Override
    public void deleteTrabajadorById(Long id) {
        trabajadorRepository.deleteById(id);
    }

}
