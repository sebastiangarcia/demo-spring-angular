package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.WhCategoriaProductoMapper;
import cl.exe.curanco.api.v1.model.WhCategoriaProductoDTO;
import cl.exe.curanco.domain.WhCategoriaProducto;
import cl.exe.curanco.repositories.WhCategoriaProductoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WhCategoriaProductoServiceImpl implements WhCategoriaProductoService {

    private final WhCategoriaProductoMapper whCategoriaProductoMapper;
    private final WhCategoriaProductoRepository whCategoriaProductoRepository;

    public WhCategoriaProductoServiceImpl(WhCategoriaProductoMapper whCategoriaProductoMapper, WhCategoriaProductoRepository whCategoriaProductoRepository) {
        this.whCategoriaProductoMapper = whCategoriaProductoMapper;
        this.whCategoriaProductoRepository = whCategoriaProductoRepository;
    }

    @Override
    public List<WhCategoriaProductoDTO> getAllWhCategoriaProductos() {

        return whCategoriaProductoRepository.findAll()
                .stream()
                .map(whCategoriaProductoMapper::whCategoriaProductoToWhCategoriaProductoDTO)
                .collect(Collectors.toList());
    }

    @Override
    public WhCategoriaProductoDTO getWhCategoriaProductoById(Long id) {
        //return whCategoriaProductoMapper.whCategoriaProductoToWhCategoriaProductoDTO(whCategoriaProductoRepository.findById(id));
        return whCategoriaProductoRepository.findById(id)
                .map(whCategoriaProductoMapper::whCategoriaProductoToWhCategoriaProductoDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public WhCategoriaProductoDTO getWhCategoriaProductoByNombre(String nombre) {
        return whCategoriaProductoMapper.whCategoriaProductoToWhCategoriaProductoDTO(whCategoriaProductoRepository.findByNombre(nombre));
    }

    @Override
    public WhCategoriaProductoDTO createNewWhCategoriaProducto(WhCategoriaProductoDTO whCategoriaProductoDTO) {
        WhCategoriaProducto whCategoriaProducto = whCategoriaProductoMapper.whCategoriaProductoDtoToWhCategoriaProducto(whCategoriaProductoDTO);
        return saveAndReturnDTO(whCategoriaProducto);
    }

    private WhCategoriaProductoDTO saveAndReturnDTO(WhCategoriaProducto whCategoriaProducto) {
        WhCategoriaProducto savedWhCategoriaProducto = whCategoriaProductoRepository.save(whCategoriaProducto);
        return whCategoriaProductoMapper.whCategoriaProductoToWhCategoriaProductoDTO(savedWhCategoriaProducto);
    }

    @Override
    public WhCategoriaProductoDTO saveWhCategoriaProductoByDTO(Long id, WhCategoriaProductoDTO whCategoriaProductoDTO) {
        WhCategoriaProducto whCategoriaProducto = whCategoriaProductoMapper.whCategoriaProductoDtoToWhCategoriaProducto(whCategoriaProductoDTO);
        whCategoriaProducto.setId(id);
        return saveAndReturnDTO(whCategoriaProducto);
    }

    @Override
    public void deleteWhCategoriaProductoById(Long id) {
        whCategoriaProductoRepository.deleteById(id);
    }

}
