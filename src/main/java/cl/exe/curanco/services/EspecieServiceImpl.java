package cl.exe.curanco.services;

import cl.exe.curanco.api.v1.mapper.EspecieMapper;
import cl.exe.curanco.api.v1.model.EspecieDTO;
import cl.exe.curanco.domain.Especie;
import cl.exe.curanco.repositories.EspecieRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EspecieServiceImpl implements EspecieService {

    private final EspecieMapper especieMapper;
    private final EspecieRepository especieRepository;

    public EspecieServiceImpl(EspecieMapper especieMapper, EspecieRepository especieRepository) {
        this.especieMapper = especieMapper;
        this.especieRepository = especieRepository;
    }

    @Override
    public List<EspecieDTO> getAllEspecies() {

        return especieRepository.findAll()
                .stream()
                .map(especieMapper::especieToEspecieDTO)
                .collect(Collectors.toList());
    }

    @Override
    public EspecieDTO getEspecieById(Long id) {
        //return especieMapper.especieToEspecieDTO(especieRepository.findById(id));
        return especieRepository.findById(id)
                .map(especieMapper::especieToEspecieDTO)
                .orElseThrow(RuntimeException::new); // todo implementar una mejor exepción
    }

    @Override
    public EspecieDTO getEspecieByNombre(String nombre) {
        return especieMapper.especieToEspecieDTO(especieRepository.findByNombre(nombre));
    }

    @Override
    public EspecieDTO createNewEspecie(EspecieDTO especieDTO) {
        Especie especie = especieMapper.especieDtoToEspecie(especieDTO);
        return saveAndReturnDTO(especie);
    }

    private EspecieDTO saveAndReturnDTO(Especie especie) {
        Especie savedEspecie = especieRepository.save(especie);
        return especieMapper.especieToEspecieDTO(savedEspecie);
    }

    @Override
    public EspecieDTO saveEspecieByDTO(Long id, EspecieDTO especieDTO) {
        Especie especie = especieMapper.especieDtoToEspecie(especieDTO);
        especie.setId(id);
        return saveAndReturnDTO(especie);
    }

    @Override
    public void deleteEspecieById(Long id) {
        especieRepository.deleteById(id);
    }

}
