package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.Operacion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperacionRepository extends JpaRepository<Operacion, Long> {
    Operacion findByNombre(String nombre);
}
