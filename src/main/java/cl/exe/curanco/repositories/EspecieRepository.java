package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.Especie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EspecieRepository extends JpaRepository<Especie, Long> {
    Especie findByNombre(String nombre);
}
