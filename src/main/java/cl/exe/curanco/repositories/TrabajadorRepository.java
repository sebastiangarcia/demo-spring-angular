package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.Trabajador;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrabajadorRepository extends JpaRepository<Trabajador, Long> {
    Trabajador findByRut(String rut);
}
