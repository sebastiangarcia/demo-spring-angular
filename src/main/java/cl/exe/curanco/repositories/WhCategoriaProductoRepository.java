package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.WhCategoriaProducto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WhCategoriaProductoRepository extends JpaRepository<WhCategoriaProducto, Long> {
    WhCategoriaProducto findByNombre(String nombre);
}
