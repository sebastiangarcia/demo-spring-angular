package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.TipoMaquina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoMaquinaRepository extends JpaRepository<TipoMaquina, Long> {
    TipoMaquina findByNombre(String nombre);
}