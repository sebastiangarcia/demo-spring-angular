package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.EstadoMaquina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstadoMaquinaRepository extends JpaRepository<EstadoMaquina, Long> {
    EstadoMaquina findByNombre(String nombre);
}
