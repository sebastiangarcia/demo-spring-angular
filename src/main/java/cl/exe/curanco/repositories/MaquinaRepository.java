package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.Maquina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaquinaRepository extends JpaRepository<Maquina, Long> {
    Maquina findByNombre(String nombre);
}
