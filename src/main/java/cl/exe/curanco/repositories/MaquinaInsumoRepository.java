package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.MaquinaInsumo;
import cl.exe.curanco.domain.MaquinaInsumoId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaquinaInsumoRepository extends JpaRepository<MaquinaInsumo, Long> {
    void deleteById(MaquinaInsumoId id);
}
