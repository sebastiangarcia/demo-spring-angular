package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.WhProducto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WhProductoRepository extends JpaRepository<WhProducto, Long> {
    WhProducto findByNombre(String nombre);
}
