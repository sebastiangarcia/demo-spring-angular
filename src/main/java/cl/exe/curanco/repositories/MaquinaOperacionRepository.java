package cl.exe.curanco.repositories;

import cl.exe.curanco.domain.MaquinaOperacion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaquinaOperacionRepository extends JpaRepository<MaquinaOperacion, Long> {

}
